from django.db import models


# Create your models here.

class Folders(models.Model):
    local_folder = models.TextField()
    remote_folder = models.TextField()
    remote_crypt = models.TextField()
    depth = models.PositiveIntegerField(default=2)
