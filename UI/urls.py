from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ffs', views.ffs, name='ffs'),
    path('listRemotes', views.listRemotes, name='listRemotes'),
    path('spaceUsed', views.spaceUsed, name='spaceUsed'),
    path('delete', views.deleteFolders),
    path('addfolder/<str:local_folder>/<str:remote_folder>/<str:remote_crypt>/<int:depth>', views.addfolder),
]

