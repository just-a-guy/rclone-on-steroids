import hashlib, os
import json
from configparser import ConfigParser
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
import logging, logging.handlers
import subprocess
import sys
import time

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler

# Create your views here.
from .Helpers.MonitorFolder import MonitorFolder
from .Helpers.StoppableThread import StoppableThread
from .models import Folders
from base64 import b64decode


threadedWatchers = {}

config = ConfigParser()

filename = os.path.join(os.path.dirname(__file__), 'config/settings.ini')
config.read(filename)
logfolder = config.get('loginfo', 'logfolder')

handler = logging.handlers.WatchedFileHandler(
    os.environ.get("LOGFILE", "{}/main.log".format(logfolder)))

formatter = logging.Formatter(logging.BASIC_FORMAT)
handler.setFormatter(formatter)
root = logging.getLogger()
root.setLevel(os.environ.get("LOGLEVEL", "INFO"))
root.addHandler(handler)

def setup():
    print("Making directory for logging")
    currdir = os.path.join(os.getcwd(), 'monitorUpload')

    if not currdir:
        os.mkdir(currdir)
        print("Made the log folder {}".format(currdir))
    else:
        print("Log folder exists.....")

    return currdir


def deleteFolders(self):
    Folders.objects.all().delete()

    return HttpResponse("Deleted folders.")


def addfolder(self, local_folder, remote_folder, remote_crypt, depth):
    try:
        remote_crypt = b64decode(remote_crypt).decode("utf-8")
    except:
        return HttpResponse(
            '<h1>Unable to decode from base 64 <br/> {}'.format(remote_crypt))

    try:
        local_folder = b64decode(local_folder).decode("utf-8")
    except:
        return HttpResponse(
            '<h1>Unable to decode from base 64 <br/> {}'.format(local_folder))

    try:
        remote_folder = b64decode(remote_folder).decode("utf-8")
    except:
        return HttpResponse(
            '<h1>Unable to decode from base 64 <br/> {}'.format(remote_folder))

    if depth <=0 or depth > 4:
        return HttpResponse(
            '<h1>The depth given {} is too much to tree map. <br/>'.format(depth))

    if not os.access(local_folder, os.W_OK):
        return HttpResponse(
            '<h4>Error: The folder {} was not accessible for monitoring. Permission issues.</h4>'.format(
                str(local_folder)))

    if Folders.objects.filter(local_folder=local_folder).exists():
        return HttpResponse(
            '<h4>Nope: The local folder {} was already setup with remote crypt folder {} and remote folder {}</h4>'.format(
                local_folder, remote_crypt, remote_folder))

    folder = Folders.objects.create(local_folder=local_folder)
    folder.remote_folder = remote_folder
    folder.remote_crypt = remote_crypt
    folder.local_folder = local_folder
    folder.depth = depth
    folder.save()

    return HttpResponse(
        '<h4>Success: The local folder {} was setup with remote crypt folder {} and remote folder {}</h4>'.format(
            local_folder, remote_crypt, remote_folder))


def ffs(self):
    return HttpResponse(
        '<h4>Success: The local folder {} was setup with remote crypt folder {} and remote folder {}</h4>')

def index(self):

    logging.info("tesintg the damn logging")

    try:
        allfolders = Folders.objects.all()
    except Folders.DoesNotExist:
        allfolders = None

    response = []

    for folderinfo in allfolders:

        strmd5 = hashlib.md5(folderinfo.local_folder.encode('utf-8')).hexdigest()

        if strmd5 in threadedWatchers:
            response.append("Skipping '{}' with remote folder '{}' and remote crypt '{}' as its already being " \
                        "monitored<br/>".format(folderinfo.local_folder, folderinfo.remote_folder, folderinfo.remote_crypt))
            continue

        w = MonitorFolder(folderinfo, logfolder)

        x = StoppableThread(function=w)
        threadedWatchers[strmd5] = x
        x.start()

        response.append("Monitoring {} which is mapped to remote crypt {} and its folder {}<br/>A total of {} folders".format(
            folderinfo.local_folder, folderinfo.remote_crypt, folderinfo.remote_folder, len(allfolders)))

    if len(allfolders) > 0:
        return JsonResponse({'status': 'true', 'details': response})
    else:
        return JsonResponse({'status': 'false', 'details': ['No folders to be monitored.']})


def spaceUsed(self):
    out = subprocess.check_output(['vnstat', '-s', '--json'])
    out = json.loads(out.decode('utf-8'))

    return JsonResponse({'status': 'true', 'details': {
        'today': out['interfaces'][0]['traffic']['days'][0]['tx'],
        'dailyLimit': 750 * 1024 * 1024 * 1024,
        'monthlyTotal': out['interfaces'][0]['traffic']['months'][0]['tx'],
        'monthlyLimit': 50 * 1024 * 1024 * 1024 * 1024
    }})



def listRemotes(self):
    # out = subprocess.check_output(['rclone', 'listremotes'])
    out = """Unlimited:
gcache:
gcrypt:
local-mounted:"""
    out = out.split('\n')
    print(out)
    return JsonResponse({'status': True, 'details': {'listOfRemotes' : out}})