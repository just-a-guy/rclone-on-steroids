import collections
import os, hashlib, subprocess, time, datetime
import logging
import traceback
from shlex import quote


def du(path):
    """disk usage in human readable format (e.g. '2,1GB')"""
    # logging.info(subprocess.check_output(['du', '-k', path]).split()[0].decode('utf-8'))
    return subprocess.check_output(['du', '-sk', path]).split()[0].decode('utf-8')


class MonitorFolder:
    TIMESTAMPED = '__timestamped__'
    TIME_BEFORE_UPLOAD = 60

    def __init__(self, folderinfo, logdir):

        self.folderobject = folderinfo
        self.sizeMonitor = {}
        self.processing = []
        self.logFolder = logdir
        self.folderList = []
        self.uploading = []
        self.ignored_list = ['__size__', '_UNPACK_', MonitorFolder.TIMESTAMPED]
        # self.folders_being_ignored = next(os.walk(self.folderobject.local_folder))[1]

        self.folderList = self.scanrec(self.folderobject.local_folder, self.folderobject.depth)
        self.folder_dir_struct = {}

        # for eachfolder in self.folderList:
        #     strmd5 = hashlib.md5(eachfolder.encode('utf-8')).hexdigest()
        #     self.sizeMonitor[strmd5] = du(eachfolder)

        self.folder_dir_struct = self.make_list_to_dict(self.folderList, self.folderobject.local_folder)

    def make_list_to_dict(self, lst, base_path):

        ignored_list = self.ignored_list
        dct = {}
        for item in lst:
            item.replace(base_path, '')
            p = dct
            for x in item.split('/'):
                p = p.setdefault(x, {})

        number_of_ignores_needed = len(base_path.strip("/").split("/"))
        ffs = list(dct[''].values())[0]

        while number_of_ignores_needed - 1 > 0:
            ffs = list(ffs.values())[0]
            number_of_ignores_needed = number_of_ignores_needed - 1

        def foldersize(full_root, json_rep):

            for main_folder in list(json_rep.keys()):
                if any(x in main_folder for x in ignored_list): continue
                thesize = du(os.path.join(full_root, main_folder))
                json_rep[main_folder]['__size__'] = thesize
                foldersize(os.path.join(full_root, main_folder), json_rep[main_folder])

            return json_rep

        return foldersize(base_path, ffs)

    def scanrec(self, root, depth_top_be_considered=1):
        rval = []

        def do_scan(start_dir, output, depth=0):
            for f in os.listdir(start_dir):
                ff = os.path.join(start_dir, f)
                if os.path.isdir(ff):
                    output.append(ff)
                    if depth < depth_top_be_considered:
                        do_scan(ff, output, depth + 1)

        do_scan(root, rval, 0)
        return rval

    def triggerUpload(self, folder, base_path):
        fullpath = folder
        now = datetime.datetime.now()
        # checking for date folder
        strmd5 = hashlib.md5(folder.encode('utf-8')).hexdigest()
        timefoldername = now.strftime("%Y-%m-%d")

        logfolder_to_be_operated = self.logFolder
        try:
            if not os.path.exists("{}/{}".format(self.logFolder, timefoldername)):
                os.mkdir("{}/{}".format(self.logFolder, timefoldername))
        except:
            logging.info("could not create log dir, dumping log in {}/{}".format(self.logFolder, timefoldername))

        # if not os.path.exists("{}/{}".format(self.logFolder, timefoldername)):
        #     logging.info("Log folder doesnt exist, creating. {}/{}".format(self.logFolder, timefoldername))
        #     os.mkdir("{}/{}".format(self.logFolder, timefoldername))

        # fixing the remote folder's trailing slash since we need one
        remote_folder = self.folderobject.remote_folder.rstrip('/')
        remote_sub_folder = folder.replace(base_path, '').strip('/')

        format1 = quote(fullpath)
        format2 = quote("{}:{}/{}".format(self.folderobject.remote_crypt, remote_folder, remote_sub_folder))

        logfilename = "{} - {}".format(folder.split('/')[-2], folder.split('/')[-1])
        format3 = quote(logfilename)

        command = "nohup rclone copy {} {} -v --stats=2s > {}/{}/{}.log 2>&1 &". \
            format(format1, format2, logfolder_to_be_operated, timefoldername, format3)

        logging.info(command)

        os.system(command)
        # self.processing.append(strmd5)
        # del self.sizeMonitor[strmd5]
        logging.info("now executing : " + command)

    def process_folder_check(self, json_blob, full_path, old_json):

        # logging.info("*****")
        # logging.info(json_blob, full_path, old_json)
        for root_folder in list(json_blob.keys()):

            if any(x in root_folder for x in self.ignored_list): continue
            # logging.info("processing : ", root_folder)

            newsize = json_blob[root_folder]['__size__']

            # now we're checking if the root_folder actually existsr in the oldsize, if it doesn't it means a new
            # folder was added and we need to monitor it
            if root_folder not in old_json:
                logging.info(" Folder {} was not found, newely created".format(root_folder))
                self.sizeMonitor[os.path.join(full_path, root_folder)] = {}
                self.sizeMonitor[os.path.join(full_path, root_folder)]['__size__'] = newsize
                logging.info(" full path for folder newely created {}".format(os.path.join(full_path, root_folder)))
                self.update_folder_dir_struct(os.path.join(full_path, root_folder), 0)
                continue

            oldsize = old_json[root_folder]['__size__']

            # logging.info("==--==", oldsize, newsize)
            # logging.info("processing {} oldsize : {}, newsize: {}".format(root_folder, oldsize, newsize))

            if oldsize != newsize:
                if len(json_blob[root_folder]) == 1:
                    logging.info(oldsize, newsize)
                    if int(oldsize) > int(newsize):
                        logging.info("Skipping adding folder {} because new size was smaller, oldsize : {}, newsize: {}".
                              format(os.path.join(full_path, root_folder), oldsize, newsize))
                        continue

                    # make sure that if it already being monitored, dont need to add it again, but update the time
                    if os.path.join(full_path, root_folder) not in self.sizeMonitor:
                        self.sizeMonitor[os.path.join(full_path, root_folder)] = {}
                        self.sizeMonitor[os.path.join(full_path, root_folder)]['__size__'] = newsize
                    else:
                        # the size compare says to only update the size if its bigger.
                        if self.sizeMonitor[os.path.join(full_path, root_folder)]['__size__'] != newsize:
                            self.sizeMonitor[os.path.join(full_path, root_folder)][
                                MonitorFolder.TIMESTAMPED] = time.time()
                            self.sizeMonitor[os.path.join(full_path, root_folder)]['__size__'] = newsize

            if len(json_blob[root_folder]) > 0:
                self.process_folder_check(json_blob[root_folder], os.path.join(full_path, root_folder),
                                          old_json[root_folder])

    def update_folder_dir_struct(self, folderpath, size):

        def update(d, u):
            for k, v in u.items():
                if isinstance(v, collections.Mapping):
                    d[k] = update(d.get(k, {}), v)
                else:
                    d[k] = v
            return d

        logging.info("************** folder path *****************\n")
        logging.info(folderpath)
        ffs = self.make_list_to_dict([folderpath], self.folderobject.local_folder)
        logging.info(" ---- made to list ----")
        logging.info(ffs)
        # logging.info(" ---- folder_dir_struct before update ----")
        # logging.info(self.folder_dir_struct)
        # logging.info("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n")
        # logging.info("updated\n")
        # logging.info(self.folder_dir_struct)
        self.folder_dir_struct = update(self.folder_dir_struct, ffs)
        logging.info("*******************************************\n")

    def run(self):

        logging.info("Now checking dir {}".format(self.folderobject.local_folder))
        while True:
            time.sleep(5)
            now_folders = self.scanrec(self.folderobject.local_folder, self.folderobject.depth)
            # folders = [x for x in folders if x not in self.folders_being_ignored]

            now_dir = self.make_list_to_dict(now_folders, self.folderobject.local_folder)

            logging.info(now_dir)

            try:

                self.process_folder_check(now_dir, self.folderobject.local_folder, self.folder_dir_struct)
                logging.info("-------- Being Upload --------")
                logging.info(self.sizeMonitor)
                logging.info("------------------------------")

                for key in list(self.sizeMonitor.keys()):

                    if MonitorFolder.TIMESTAMPED in self.sizeMonitor[key]:
                        logging.info(str(time.time()) + " " + str(self.sizeMonitor[key][MonitorFolder.TIMESTAMPED]) + " " +
                              str(time.time() - self.sizeMonitor[key][MonitorFolder.TIMESTAMPED]))

                        if (time.time() - self.sizeMonitor[key][
                            MonitorFolder.TIMESTAMPED]) >= MonitorFolder.TIME_BEFORE_UPLOAD:

                            if key not in self.uploading:
                                self.uploading.append(key)
                                self.update_folder_dir_struct(key, self.sizeMonitor[key]['__size__'])
                                del (self.sizeMonitor[key])

                    else:
                        self.sizeMonitor[key][MonitorFolder.TIMESTAMPED] = time.time()

                # now we are uploading
                for each in self.uploading:
                    self.triggerUpload(each, self.folderobject.local_folder)

                self.uploading = []

            except Exception as e:
                logging.info("exception .....======")
                logging.info(e)
                # logging.info(traceback.logging.info_exc())
                logging.error("---- BIG ERROR ----", exc_info=True)
                pass

    def stop(self):
        pass
