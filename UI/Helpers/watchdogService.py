import time, os
import hashlib
import subprocess

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


def du(path):
    """disk usage in human readable format (e.g. '2,1GB')"""
    return subprocess.check_output(['du', '-sh', path]).split()[0].decode('utf-8')


class Watcher:
    DIRECTORY_TO_WATCH = ""

    def __init__(self, folder):
        self.observer = Observer()
        self.DIRECTORY_TO_WATCH = folder

    def run(self):
        print("RUNNING WATCHER START")
        event_handler = Handler(self.DIRECTORY_TO_WATCH)
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Error")

        self.observer.join()

    def stop(self):
        print("RUNNING WATCHER STOP")
        self.observer.stop()


class Handler(FileSystemEventHandler):

    def __init__(self, directory):

        self.directory = directory
        self.sizeMonitor = {}

    def triggerUpload(self, folder):
        print("---------------")

    def handleCreated(self):
        folders = next(os.walk(self.directory))[1]

        print("--------")
        print(folders)

        for folder in folders:

            strmd5 = hashlib.md5(folder.encode('utf-8')).hexdigest()
            newsize = du(self.directory + "/" + folder)

            if strmd5 in self.sizeMonitor:
                print(" already being monitored ")
                oldsize = self.sizeMonitor[strmd5]

                if oldsize != newsize:
                    self.sizeMonitor[strmd5] = newsize
                else:
                    self.triggerUpload(folder)

            else:
                print(" now being monitored ")
                self.sizeMonitor[strmd5] = newsize

    def on_any_event(self, event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("Received created event - %s." % event.src_path)

            self.handleCreated()

            # print(test)

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            pass
