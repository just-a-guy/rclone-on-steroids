import threading, logging


class StoppableThread(threading.Thread):
    def __init__(self, function):
        self.running = False
        self.function = function
        super(StoppableThread, self).__init__()

    def start(self):
        logging.info("RUNNING THERAD START\n")
        self.running = True
        super(StoppableThread, self).start()

    def run(self):
        while self.running:
            self.function.run()

    def stop(self):
        logging.info("RUNNING THERAD STOP\n")
        self.running = False
        self.function.stop()

    def getMonitoredFolder(self):
        self.function.getFolderMonitored()